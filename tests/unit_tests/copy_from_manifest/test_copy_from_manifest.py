#
# Copyright 2019 Russell Smiley
#
# This file is part of distribution_package.
#
# distribution_package is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# distribution_package is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with distribution_package.  If not, see <http://www.gnu.org/licenses/>.
#

import unittest.mock

from distribution_package.copy_from_manifest.app import main

# !! DO NOT DELETE. Used by unittest.mock below
import distribution_package.copy_from_manifest


class TestCopyFromManifest(unittest.TestCase):

    def test_execution(self):
        """
        Test that data from multiple manifests is aggregated correctly.
        """

        mock_manifest_paths = ['file1', 'file2']
        mock_project_root = 'some/path'

        mock_arguments = [
            '-m',
            *mock_manifest_paths,
            '-r',
            mock_project_root,
        ]

        with unittest.mock.patch('distribution_package.copy_from_manifest.app.combine_manifests') \
                as mock_combine:
            main(mock_arguments)

            mock_combine.assert_called_once_with(mock_manifest_paths, mock_project_root)


if __name__ == '__main__':
    unittest.main()
