#
# Copyright 2018 Russell Smiley
#
# This file is part of distribution_package.
#
# distribution_package is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# distribution_package is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with distribution_package.  If not, see <http://www.gnu.org/licenses/>.
#

"""
Test ``TarPackage`` class.
"""

import os
import unittest.mock

from .utility import \
    AppendTestData, \
    MockCompressedOpen, \
    PackageTestData

from distribution_package.make_package.tar_package import \
    TarPackage, \
    tarfile

import distribution_package.make_package.tar_package


class TestTarPackage(unittest.TestCase):
    """
    Test ``TarPackage`` class.
    """

    def setUp(self):
        self.data = PackageTestData()

    def test_append_disabled_creates_nonexistent_file(self):
        append_mode = False
        mock_tar_file = unittest.mock.create_autospec(tarfile.TarFile)

        with unittest.mock.patch('tarfile.open',
                                 return_value=mock_tar_file) as mock_tar_open:
            with unittest.mock.patch(
                    'distribution_package.make_package.tar_package.TarPackage'
                    '._TarPackage__do_build'):
                package_under_test = TarPackage(self.id(), '.', append_mode)

                package_under_test.build(self.data.files_to_package)

        mock_tar_open.assert_called_with(name=self.id(),
                                         mode='w:gz')

    def test_subdir_is_discovered(self):
        expected_subdir_found_count = 1

        mock_tar_file = unittest.mock.create_autospec(tarfile.TarFile)

        with unittest.mock.patch('os.path.isfile',
                                 side_effect=PackageTestData.mock_os_isfile), \
             unittest.mock.patch('tarfile.open',
                                 return_value=mock_tar_file):
            package_under_test = TarPackage(self.id(), '.', False)

            package_under_test.build(self.data.files_to_package)

        self.assertEqual(expected_subdir_found_count, self.data.subdir_found_count)

    def test_added_files(self):
        with unittest.mock.patch('os.path.isfile',
                                 side_effect=PackageTestData.mock_os_isfile), \
             unittest.mock.patch('tarfile.open',
                                 new_callable=MockCompressedOpen) as mock_zipfile, \
                unittest.mock.patch('distribution_package.make_package.tar_package.ChangeDirectory',
                                    autospec=True):
            package_under_test = TarPackage(self.id(), '.', False)
            package_under_test.build(self.data.files_to_package)

            self.assertEqual(self.data.expected_packaged_files,
                             mock_zipfile.fileObject.writtenFiles)


class TestTarPackageappend_mode(unittest.TestCase):
    """
    Test ``TarPackage`` append mode.
    """

    def setUp(self):
        self.data = PackageTestData()

    def test_append_creates_nonexistent_file(self):
        """
        Append mode on a non-existent file silently creates the file.
        :return:
        """
        append_mode = True

        with unittest.mock.patch('os.path.isfile',
                                 return_value=False) as mockOsIsfile:
            with unittest.mock.patch(
                    'distribution_package.make_package.tar_package.TarPackage'
                    '._TarPackage__open_for_write'
                    '') as \
                    mock_write:
                package_under_test = TarPackage(self.id(), '.', append_mode)
                package_under_test.build(self.data.files_to_package)

                # If "openForWrite" is called then the file is created (or overwritten,
                # but in this case the file
                # doesn't exist).
                mock_write.assert_called_once_with(self.data.files_to_package)

    def test_append_to_file(self):
        append_mode = True
        mock_tar_file = unittest.mock.create_autospec(tarfile.TarFile)

        with unittest.mock.patch('tarfile.open',
                                 return_value=mock_tar_file) as mock_tar_open, \
                unittest.mock.patch('os.path.isfile',
                                    return_value=False), \
                unittest.mock.patch(
                    'distribution_package.make_package.tar_package.TarPackage'
                    '._TarPackage__do_build'):
            package_under_test = TarPackage(self.id(), '.', append_mode)

            package_under_test.build(self.data.files_to_package)

            mock_tar_open.assert_called_with(name=self.id(),
                                             mode='w:gz')


class TestTarPackageAppend(unittest.TestCase):
    """
    Test an actual append operation.
    """

    def test_append_file(self):
        with AppendTestData() as append_data:
            package_file = os.path.join(append_data.temp_directory, self.id())

            package_under_test = TarPackage(package_file, append_data.temp_directory, False)
            package_under_test.build({'file1'})

            self.assertTrue(os.path.exists(package_file))

            package_under_test.append = True
            package_under_test.build({'file2'})

            with tarfile.open(package_file, 'r:gz') as extract_tar:
                info = set(extract_tar.getnames())

            self.assertEqual({'file1', 'file2'}, info)


if __name__ == '__main__':
    unittest.main()
