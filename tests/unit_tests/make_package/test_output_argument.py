#
# Copyright 2018 Russell Smiley
#
# This file is part of distribution_package.
#
# distribution_package is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# distribution_package is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with distribution_package.  If not, see <http://www.gnu.org/licenses/>.
#

import os
import unittest

from distribution_package.make_package.options import UserOptions


class MyTestOutputArgument(unittest.TestCase):
    def test_default_output(self):
        expected_default_output = '.'

        options_under_test = UserOptions()
        options_under_test.parse_arguments(list())

        actual_path = options_under_test.tar_package_path

        actual_output = os.path.dirname(actual_path)

        self.assertEqual(expected_default_output, actual_output)

    def test_short_form_output(self):
        expected_output = os.path.join('some', 'path')

        options_under_test = UserOptions()
        options_under_test.parse_arguments(['-o', expected_output])

        actual_path = options_under_test.tar_package_path

        actual_output = os.path.dirname(actual_path)

        self.assertEqual(expected_output, actual_output)

    def test_long_form_output(self):
        expected_output = os.path.join('some', 'path')

        options_under_test = UserOptions()
        options_under_test.parse_arguments(['--output', expected_output])

        actual_path = options_under_test.tar_package_path

        actual_output = os.path.dirname(actual_path)

        self.assertEqual(expected_output, actual_output)


if __name__ == '__main__':
    unittest.main()
