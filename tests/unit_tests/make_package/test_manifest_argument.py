#
# Copyright 2018 Russell Smiley
#
# This file is part of distribution_package.
#
# distribution_package is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# distribution_package is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with distribution_package.  If not, see <http://www.gnu.org/licenses/>.
#

"""
Test package options relating to manifest specification.
"""

import unittest

from distribution_package.make_package.options import UserOptions


class TestManifestArgument(unittest.TestCase):

    def test_default_manifest(self):
        """
        Manifest defaults
        """
        expected_default_manifests = ['manifest.yml']

        options_under_test = UserOptions()
        options_under_test.parse_arguments(list())

        actual_manifests = options_under_test.manifest_path

        self.assertEqual(expected_default_manifests, actual_manifests)

    def test_short_form_manifest(self):
        """
        Short form manifest option.
        """
        manifest_file = 'some-manifest.yml'
        expected_manifests = [manifest_file]

        options_under_test = UserOptions()
        options_under_test.parse_arguments(['-m', manifest_file])

        actual_manifests = options_under_test.manifest_path

        self.assertEqual(expected_manifests, actual_manifests)

    def test_long_form_manifest(self):
        """
        Long form manifest option.
        """
        manifest_file = 'some-manifest.yml'
        expected_manifests = [manifest_file]

        options_under_test = UserOptions()
        options_under_test.parse_arguments(['--manifests', manifest_file])

        actual_manifests = options_under_test.manifest_path

        self.assertEqual(expected_manifests, actual_manifests)

    def test_multiplem_manifests(self):
        """
        Long form manifest option.
        """
        expected_manifests = ['file1.yml', 'file2.yml']

        input_arguments = ['--manifests'] + expected_manifests
        options_under_test = UserOptions()
        options_under_test.parse_arguments(input_arguments)

        actual_manifests = options_under_test.manifest_path

        self.assertEqual(expected_manifests, actual_manifests)

    def test_manifest_with_output(self):
        """
        The user specified manifest must be used independently of other arguments.
        """
        expected_manifest_argument = ['some/manifest.yml']
        output_argument = 'another/path'

        input_arguments = [
            '-m',
            expected_manifest_argument[0],
            '-o',
            output_argument,
        ]

        options_under_test = UserOptions()
        options_under_test.parse_arguments(input_arguments)

        actual_manifests = options_under_test.manifest_path

        self.assertEqual(expected_manifest_argument, actual_manifests)


if __name__ == '__main__':
    unittest.main()
