#
# Copyright 2018 Russell Smiley
#
# This file is part of packager.
#
# packager is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# packager is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with packager.  If not, see <http://www.gnu.org/licenses/>.
#

"""
Test ``Package`` class.
"""

import unittest.mock

from distribution_package.make_package.options import UserOptions
from packageManifest import Manifest

from distribution_package.make_package.package import Package

# DO NOT DELETE. Used by unittest.mock.patch below.
import distribution_package.make_package.package


class TestPackage(unittest.TestCase):

    def setUp(self):
        self.mock_package_options = unittest.mock.create_autospec(UserOptions)
        self.mock_package_options.append = False
        self.mock_package_options.project_root = '.'

    def test_tar_package(self):
        """
        When tar package is selected, the tar package object build method is called.
        """
        self.mock_package_options.is_tar_file = True
        self.mock_package_options.is_zip_file = False

        with unittest.mock.patch('distribution_package.make_package.package.TarPackage',
                                 autospec=True) as mock_tar_package, \
                unittest.mock.patch('packageManifest.Manifest',
                                    autospec=True) as mockManifest:
            package_under_test = Package(mockManifest, self.mock_package_options)
            package_under_test.build()

            mockManifest.apply.assert_called_once()

            mock_tar_package_object = mock_tar_package.return_value
            mock_tar_package_object.build.assert_called_once()

    def test_zip_package(self):
        """
        When zip package is selected, the zip package object build method is called.
        """
        self.mock_package_options.is_tar_file = False
        self.mock_package_options.is_zip_file = True

        with unittest.mock.patch('distribution_package.make_package.package.ZipPackage',
                                 autospec=True) as mock_zip_package, \
                unittest.mock.patch('packageManifest.Manifest',
                                    autospec=True) as mock_manifest:
            package_under_test = Package(mock_manifest, self.mock_package_options)
            package_under_test.build()

            mock_manifest.apply.assert_called_once()

            mock_zip_package_object = mock_zip_package.return_value
            mock_zip_package_object.build.assert_called_once()

    def test_both_packages(self):
        """
        User is able to package both tar and zip simultaneously.
        """
        self.mock_package_options.is_tar_file = True
        self.mock_package_options.is_zip_file = True

        with unittest.mock.patch('distribution_package.make_package.package.TarPackage',
                                 autospec=True) as mock_tar_package, \
                unittest.mock.patch('distribution_package.make_package.package.ZipPackage',
                                    autospec=True) as mock_zip_package, \
                unittest.mock.patch('packageManifest.Manifest',
                                    autospec=True) as mock_manifest:
            package_under_test = Package(mock_manifest, self.mock_package_options)
            package_under_test.build()

            mock_manifest.apply.assert_called_once()

            mock_tar_package_object = mock_tar_package.return_value
            mock_tar_package_object.build.assert_called_once()

            mock_zip_package_object = mock_zip_package.return_value
            mock_zip_package_object.build.assert_called_once()


if __name__ == '__main__':
    unittest.main()
