#
# Copyright 2018 Russell Smiley
#
# This file is part of distribution_package.
#
# distribution_package is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# distribution_package is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with distribution_package.  If not, see <http://www.gnu.org/licenses/>.
#

import os
import unittest

from distribution_package.make_package.options import UserOptions


class TestProjectRootArgument(unittest.TestCase):
    def setUp(self) -> None:
        self.options_under_test = UserOptions()

    def test_default(self):
        expected_project_root = '.'

        self.options_under_test.parse_arguments(list())
        actual_project_root = self.options_under_test.project_root

        self.assertEqual(expected_project_root, actual_project_root)

    def test_short_form_project_root(self):
        expected_path = os.path.join('some', 'path')

        self.options_under_test.parse_arguments(['-r', expected_path])
        actual_path = self.options_under_test.project_root

        self.assertEqual(expected_path, actual_path)

    def test_long_form_project_root(self):
        expected_path = os.path.join('some', 'path')

        self.options_under_test.parse_arguments(['--project-root', expected_path])
        actual_path = self.options_under_test.project_root

        self.assertEqual(expected_path, actual_path)


if __name__ == '__main__':
    unittest.main()
