#
# Copyright 2018 Russell Smiley
#
# This file is part of distribution_package.
#
# distribution_package is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# distribution_package is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with distribution_package.  If not, see <http://www.gnu.org/licenses/>.
#

"""
Test ``ZipPackage`` class.
"""

import os
import unittest.mock

from .utility import \
    AppendTestData, \
    MockCompressedOpen, \
    PackageTestData

from distribution_package.make_package.zip_package import \
    ZipPackage, \
    zipfile

import distribution_package.make_package.zip_package


class TestZipPackage(unittest.TestCase):
    """
    Test ``ZipPackage`` class.
    """

    def setUp(self):
        self.data = PackageTestData()

    def test_append_disabled_creates_nonexistent_file(self):
        append_mode = False

        with unittest.mock.patch(
                'distribution_package.make_package.zip_package.ZipPackage._ZipPackage__do_build'), \
             unittest.mock.patch(
                 'distribution_package.make_package.zip_package.zipfile.ZipFile') as mockZipFile:
            package_under_test = ZipPackage(self.id(), '.', append_mode)

            package_under_test.build(self.data.files_to_package)

        mockZipFile.assert_called_with(self.id(),
                                       mode='w')

    def test_non_existent_file_is_discovered(self):
        expectedNotFoundCount = 1

        with unittest.mock.patch('os.path.isfile',
                                 side_effect=PackageTestData.mock_os_isfile), \
             unittest.mock.patch('distribution_package.make_package.zip_package.zipfile.ZipFile'):
            package_under_test = ZipPackage(self.id(), '.', False)

            package_under_test.build(self.data.files_to_package)

        self.assertEqual(expectedNotFoundCount, self.data.notFoundFileCount)

    def test_subdir_is_discovered(self):
        expected_subdir_found_count = 1

        with unittest.mock.patch('os.path.isfile',
                                 side_effect=PackageTestData.mock_os_isfile), \
             unittest.mock.patch('distribution_package.make_package.zip_package.zipfile.ZipFile'):
            package_under_test = ZipPackage(self.id(), '.', False)

            package_under_test.build(self.data.files_to_package)

        self.assertEqual(expected_subdir_found_count, self.data.subdir_found_count)

    def testAddedFiles(self):
        with unittest.mock.patch('os.path.isfile',
                                 side_effect=PackageTestData.mock_os_isfile), \
             unittest.mock.patch('distribution_package.make_package.zip_package.zipfile.ZipFile',
                                 new_callable=MockCompressedOpen) as mock_zipfile, \
                unittest.mock.patch('distribution_package.make_package.zip_package.ChangeDirectory',
                                    autospec=True):
            package_under_test = ZipPackage(self.id(), '.', False)
            package_under_test.build(self.data.files_to_package)

            self.assertEqual(self.data.expected_packaged_files,
                             mock_zipfile.fileObject.writtenFiles)


class TestZipPackageappend_mode(unittest.TestCase):
    """
    Test ``ZipPackage`` append mode.
    """

    def setUp(self):
        self.data = PackageTestData()

    def test_append_creates_nonexistent_file(self):
        append_mode = True

        with unittest.mock.patch('os.path.isfile', return_value=True):
            with unittest.mock.patch(
                    'distribution_package.make_package.tar_package.TarPackage'
                    '._TarPackage__do_build'):
                with unittest.mock.patch(
                        'distribution_package.make_package.zip_package.zipfile.ZipFile') as \
                        mockZipFile:
                    package_under_test = ZipPackage(self.id(), '.', append_mode)

                    package_under_test.build(self.data.files_to_package)

                    mockZipFile.assert_called_with(self.id(),
                                                   mode='a')

    def testAppendToFile(self):
        append_mode = True

        with unittest.mock.patch('os.path.isfile', return_value=False):
            with unittest.mock.patch('os.path.exists', return_value=False):
                with unittest.mock.patch(
                        'distribution_package.make_package.zip_package.ZipPackage'
                        '._ZipPackage__do_build'):
                    with unittest.mock.patch(
                            'distribution_package.make_package.zip_package.zipfile.ZipFile') as \
                            mockZipFile:
                        package_under_test = ZipPackage(self.id(), '.', append_mode)

                        package_under_test.build(self.data.files_to_package)

                        mockZipFile.assert_called_with(self.id(),
                                                       mode='w')


class TestZipPackageAppend(unittest.TestCase):
    """
    Test an actual append operation.
    """

    def test_append_file(self):
        with AppendTestData() as append_data:
            package_file = os.path.join(append_data.temp_directory, self.id())

            package_under_test = ZipPackage(package_file, append_data.temp_directory, False)
            package_under_test.build({'file1'})

            self.assertTrue(os.path.exists(package_file))

            package_under_test.append = True
            package_under_test.build({'file2'})

            with zipfile.ZipFile(package_file) as extract_zip:
                info = extract_zip.infolist()

            actual_items = {x.filename for x in info}
            self.assertEqual({'file1', 'file2'}, actual_items)


if __name__ == '__main__':
    unittest.main()
