#
# Copyright 2018 Russell Smiley
#
# This file is part of distribution_package.
#
# distribution_package is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# distribution_package is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with distribution_package.  If not, see <http://www.gnu.org/licenses/>.
#

import os
import re
import unittest

from distribution_package.make_package.options import UserOptions


class TestPackageArgument(unittest.TestCase):
    def test_default_package(self):
        expected_default_package = 'distribution-out'

        options_under_test = UserOptions()
        options_under_test.parse_arguments(list())

        tar_path = options_under_test.tar_package_path

        actual_package_file = os.path.basename(tar_path)

        self.assertIsNotNone(
            re.search(r'^{0}'.format(expected_default_package), actual_package_file))

    def testShortFormPackage(self):
        expected_package = 'my-package'

        options_under_test = UserOptions()
        options_under_test.parse_arguments(['-p', expected_package])

        tar_path = options_under_test.tar_package_path

        actual_package_file = os.path.basename(tar_path)

        self.assertIsNotNone(re.search(r'^{0}'.format(expected_package), actual_package_file))

    def testLongFormPackage(self):
        expected_package = 'my-package'

        options_under_test = UserOptions()
        options_under_test.parse_arguments(['--package', expected_package])

        tar_path = options_under_test.tar_package_path

        actual_package_file = os.path.basename(tar_path)

        self.assertIsNotNone(re.search(r'^{0}'.format(expected_package), actual_package_file))


if __name__ == '__main__':
    unittest.main()
