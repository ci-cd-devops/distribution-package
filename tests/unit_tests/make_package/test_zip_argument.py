#
# Copyright 2018 Russell Smiley
#
# This file is part of distribution_package.
#
# distribution_package is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# distribution_package is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with distribution_package.  If not, see <http://www.gnu.org/licenses/>.
#

"""
Test zip command line option.
"""

import os
import unittest

from distribution_package.make_package.options import UserOptions


class TestZipPackage(unittest.TestCase):
    """
    Test zip package command line options.
    """

    def setUp(self) -> None:
        self.options_under_test = UserOptions()

    def testDefaultDisabledPackage(self):
        self.options_under_test.parse_arguments(list())

        self.assertFalse(self.options_under_test.is_zip_file)

    def testShortEnabledPackage(self):
        expected_package = 'distribution-out.zip'

        self.options_under_test.parse_arguments(['-z'])

        self.assertTrue(self.options_under_test.is_zip_file)
        actual_package = os.path.basename(self.options_under_test.zip_package_path)
        self.assertEqual(expected_package, actual_package)

    def testLongEnabledPackage(self):
        expected_package = 'distribution-out.zip'

        self.options_under_test.parse_arguments(['--zip'])

        self.assertTrue(self.options_under_test.is_zip_file)
        actual_package = os.path.basename(self.options_under_test.zip_package_path)
        self.assertEqual(expected_package, actual_package)


if __name__ == '__main__':
    unittest.main()
