#
# Copyright 2018 Russell Smiley
#
# This file is part of packager.
#
# packager is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# packager is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with packager.  If not, see <http://www.gnu.org/licenses/>.
#

import unittest

from distribution_package.make_package.options import UserOptions


class TestAppendArgument(unittest.TestCase):

    def test_default(self):
        expected_value = False

        options_under_test = UserOptions()
        options_under_test.parse_arguments(list())

        actual_value = options_under_test.append

        self.assertEqual(expected_value, actual_value)

    def test_good_value(self):
        expected_value = True

        options_under_test = UserOptions()
        options_under_test.parse_arguments(['--append'])

        actual_value = options_under_test.append

        self.assertEqual(expected_value, actual_value)


if __name__ == '__main__':
    unittest.main()
