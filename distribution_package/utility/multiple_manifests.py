#
# Copyright 2019 Russell Smiley
#
# This file is part of distribution_package.
#
# distribution_package is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# distribution_package is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with distribution_package.  If not, see <http://www.gnu.org/licenses/>.
#

"""Combine manifests from multiple files into a single manifest."""

import typing
import yaml

from packageManifest import Manifest


def combine_manifests(manifest_paths: typing.List[str], project_root: str) -> Manifest:
    """Aggregate separate manifest files into a single entity to create a manifest from."""
    yaml_data = list()
    for this_file in manifest_paths:
        with open(this_file, 'r') as yamlFile:
            this_data = yaml.load(yamlFile,
                                  Loader=yaml.SafeLoader)

        assert isinstance(this_data, list)

        yaml_data += this_data

    this_manifest = Manifest.from_yamlData(yaml_data, project_root)

    return this_manifest
