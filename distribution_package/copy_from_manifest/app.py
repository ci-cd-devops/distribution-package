#
# Copyright 2019 Russell Smiley
#
# This file is part of distribution_package.
#
# distribution_package is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# distribution_package is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with distribution_package.  If not, see <http://www.gnu.org/licenses/>.
#

"""Flit entry point and main execution path for ``copy_from_manifest`` utility."""

import logging
import os
import shutil
import sys
import typing

from packageManifest import Manifest

from distribution_package.utility import combine_manifests

from .options import UserOptions

log = logging.getLogger('copyFromManifest')


def copy_files(this_manifest: Manifest, project_root: str, destination_dir: str):
    """Copy files specified by a manifest to a directory location."""
    log.debug('Destination directory for copy files, {0}'.format(destination_dir))
    os.makedirs(destination_dir,
                exist_ok=True)

    files_to_package = this_manifest.apply()
    for this_file in files_to_package:
        source_path = os.path.join(project_root, this_file)
        destination_path = os.path.join(destination_dir, this_file)

        log.info('src, {0}, dst, {1}'.format(source_path, destination_path))

        os.makedirs(os.path.dirname(destination_path),
                    exist_ok=True)

        shutil.copyfile(source_path, destination_path)


def main(command_line_argument: typing.List[str]):
    """Implement main execution path of hte ``copy_from_manifest`` utility."""
    log.setLevel(logging.DEBUG)

    user_options = UserOptions()
    user_options.parse_arguments(command_line_argument)

    this_manifest = combine_manifests(user_options.manifest_path, user_options.project_root)

    log.debug('Source file root, {0}'.format(user_options.project_root))

    copy_files(this_manifest, user_options.project_root, user_options.output)


def entrypoint():
    """Flit entry point to the ``copy_from_manifest`` utility."""
    main(sys.argv[1:])


if __name__ == '__main__':
    entrypoint()
