#
# Copyright 2019 Russell Smiley
#
# This file is part of distribution_package.
#
# distribution_package is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# distribution_package is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with distribution_package.  If not, see <http://www.gnu.org/licenses/>.
#

"""Implement user command line options for ``copy_from_manifest`` command."""

import argparse
import typing


class UserOptions:
    """Command line options encapsulation and parsing."""

    DEFAULT_MANIFEST_FILE = 'manifest.yml'
    DEFAULT_OUTPUT_DIR = 'manifestOutput'
    DEFAULT_PROJECT_ROOT_DIRECTORY = '.'

    def __init__(self):
        """Instantiate user defined options object for ``copy_from_manifest`` utility."""
        self.__parsed_arguments = None
        self.__parser = argparse.ArgumentParser(
            description='Copy files specified in one or more manifests into a directory, '
                        'preserving directory hierarchy of the source files. Creates the output '
                        'directory if necessary.'
        )

        self.__parser.add_argument('-m', '--manifests',
                                   default=['{0}'.format(self.DEFAULT_MANIFEST_FILE)],
                                   dest='manifest_path',
                                   help='Manifest file(s) to use to build the package. (default '
                                        '{0})'.format(self.DEFAULT_MANIFEST_FILE),
                                   nargs='*')
        self.__parser.add_argument('-o', '--output',
                                   default='{0}'.format(self.DEFAULT_OUTPUT_DIR),
                                   help='Directory to place the generated distribution files. '
                                        'Created if not present. (default {0}'.format(
                                       self.DEFAULT_OUTPUT_DIR))
        self.__parser.add_argument('-r', '--project-root',
                                   default='{0}'.format(self.DEFAULT_PROJECT_ROOT_DIRECTORY),
                                   dest='project_root',
                                   help='Project root to package files from. (default '
                                        '{0}/)'.format(self.DEFAULT_PROJECT_ROOT_DIRECTORY))

    def __getattr__(self, item: str) -> typing.Any:
        """Deliver parsed argument parameters as attributes of ``UserOptions`` class."""
        if self.__parsed_arguments is not None:
            value = getattr(self.__parsed_arguments, item)
        else:
            raise RuntimeError(
                'Command line arguments must be parsed using parse_arguments method to initialise '
                'data.')

        return value

    def parse_arguments(self, command_line_arguments: typing.List[str]):
        """Parse ``sys.argv[1:]`` style command line arguments."""
        self.__parsed_arguments = self.__parser.parse_args(command_line_arguments)
