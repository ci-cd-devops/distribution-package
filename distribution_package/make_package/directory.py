#
# Copyright 2018 Russell Smiley
#
# This file is part of distribution_package.
#
# distribution_package is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# distribution_package is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with distribution_package.  If not, see <http://www.gnu.org/licenses/>.
#

"""Implement ``ChangeDirectory`` class."""

import os


class ChangeDirectory:
    """
    Temporarily change the current file system system.

    Provisions a context manager for use in a ``with`` statement that changes the current
    directory on the file system. Leaving the scope of the ``with`` statement restores the
    current working directory that was in place previously.
    """

    def __init__(self, directory: str):
        """Instantiate the necessary parameters for preserving current working directory."""
        self.new_directory = directory
        self.previous_directory = None

    def __enter__(self):
        """Entry point for the context manager changes the current directory using ``os.chdir``."""
        self.previous_directory = os.path.realpath(
            os.path.abspath(os.path.curdir))

        # Change to the new directory\
        os.chdir(self.new_directory)

    def __exit__(self, exc_type, exc_val, exc_tb):
        """Exit point for the context manager restores the original directory."""
        os.chdir(self.previous_directory)
