import argparse
import os
import typing


class UserOptions:
    DEFAULT_MANIFEST_FILE: str
    DEFAULT_OUTPUT_DIRECTORY: str
    DEFAULT_OUT_FILE_STUB: str
    DEFAULT_PROJECT_ROOT_DIRECTORY: str

    __parsed_arguments: typing.Optional[argparse.Namespace]
    __parser: argparse.ArgumentParser

    def __init__(self): ...

    @property
    def tar_package_path(self) -> typing.Optional[str]: ...

    @property
    def zip_package_path(self) -> typing.Optional[str]: ...

    def __getattr__(self, item: str) -> typing.Any: ...

    def parse_arguments(self, command_line_arguments: typing.List[str]): ...

    #
    # These properties are fulfilled using the ``__getattr__`` method. They are described here as
    # properties to
    # assist with PyCharm type hinting.
    #
    @property
    def append(self) -> bool: ...

    @property
    def manifest_path(self) -> str: ...

    @property
    def output(self) -> str: ...

    @property
    def project_root(self) -> str: ...

    @property
    def is_tar_file(self) -> bool: ...

    @property
    def is_zip_file(self) -> bool: ...
