#
# Copyright 2018 Russell Smiley
#
# This file is part of distribution_package.
#
# distribution_package is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# distribution_package is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with distribution_package.  If not, see <http://www.gnu.org/licenses/>.
#

"""User specified packaging options."""

import argparse
import os
import typing


class UserOptions:
    """Collect parsed argument values, producing "composited" values such as file paths as necessary."""

    DEFAULT_MANIFEST_FILE = 'manifest.yml'
    DEFAULT_OUTPUT_DIRECTORY = '.'
    DEFAULT_OUT_FILE_STUB = 'distribution-out'
    DEFAULT_PROJECT_ROOT_DIRECTORY = '.'

    def __init__(self):
        """Instantiate ``make_package`` user defined options."""
        self.__parsed_arguments = None

        self.__parser = argparse.ArgumentParser(
            description='Build a distribution package from a manifest')
        self.__parser.add_argument('-a', '--append',
                                   action='store_true',
                                   default=False,
                                   dest='append',
                                   help='Append to an existing distribution package, '
                                        'if it exists. Created otherwise. ('
                                        'default disabled)')
        self.__parser.add_argument('-m', '--manifests',
                                   default=['{0}'.format(self.DEFAULT_MANIFEST_FILE)],
                                   dest='manifest_path',
                                   help='Manifest file(s) to use to build the package. (default '
                                        '{0})'.format(self.DEFAULT_MANIFEST_FILE),
                                   nargs='*')
        self.__parser.add_argument('-o', '--output',
                                   default='{0}'.format(self.DEFAULT_OUTPUT_DIRECTORY),
                                   help='Directory to place the generated distribution files. '
                                        'Created if not present. (default {0}/)'.format(
                                       self.DEFAULT_OUTPUT_DIRECTORY))
        self.__parser.add_argument('-p', '--package',
                                   default='{0}'.format(self.DEFAULT_OUT_FILE_STUB),
                                   help='Filename stub of output package, not including suffix or '
                                        'version info. (default {0})'.format(
                                       self.DEFAULT_OUT_FILE_STUB))
        self.__parser.add_argument('-r', '--project-root',
                                   default='{0}'.format(self.DEFAULT_PROJECT_ROOT_DIRECTORY),
                                   dest='project_root',
                                   help='Project root to package files from. (default {0}'
                                        '/)'.format(self.DEFAULT_PROJECT_ROOT_DIRECTORY))
        self.__parser.add_argument('-n', '--no-tar',
                                   action='store_false',
                                   default=True,
                                   dest='is_tar_file',
                                   help='Disable tar, gzip output distribution. (default enabled)')
        self.__parser.add_argument('-z', '--zip',
                                   action='store_true',
                                   default=False,
                                   dest='is_zip_file',
                                   help='Enable zip output distribution. (default disabled)')

    @property
    def tar_package_path(self) -> typing.Optional[str]:
        """Generate the tar package file output path. None if tar file output is not selected."""
        if self.is_tar_file:
            package_path = os.path.join(self.output, '{0}.tar.gz'.format(self.package))
        else:
            package_path = None

        return package_path

    @property
    def zip_package_path(self) -> typing.Optional[str]:
        """Generate the zip package file output path. None if zip file output is not selected."""
        if self.is_zip_file:
            package_path = os.path.join(self.output, '{0}.zip'.format(self.package))
        else:
            package_path = None

        return package_path

    def __getattr__(self, item: str) -> typing.Any:
        """Deliver parsed argument parameters as attributes of ``UserOptions`` class."""
        value = getattr(self.__parsed_arguments, item)

        return value

    def parse_arguments(self, command_line_arguments: typing.List[str]):
        """Parse ``sys.argv[1:]`` style command line arguments."""
        self.__parsed_arguments = self.__parser.parse_args(command_line_arguments)
